#!/bin/sh

# DON'T CHANGE THAT OR IT GET BROKEN!
D1=$(readlink -f "$0")
BINARYPATH="$(dirname "${D1}")"
cd "${BINARYPATH}"
LIBRARYPATH="$(pwd)"
# DON'T CHANGE THAT OR IT GET BROKEN!


# --- edit variables  ---
USER="philippe"							#USER WHICH THE BOT RUN AT
EMAIL="EMAIL"							#e.g ts3bot@email.com
PORT="8080"								#e.g 8080, 8079 (1024-65535) ! WANT TO START A SECOND BOT CHANGE THIS !
MASTERPW="PASSWORD"						#ADMIN PASSWORD
USERPW="PASSWORD"						#USER PASSWORD
QUERY="query"							#START BOT AS query OR noquery
SCREENNAME="musikbot_1"					#SCREEN NAME ! WANT TO START A SECOND BOT CHANGE THIS !
NUMBER="1"								#Can set which configuration should get loaded. Valid values are 1, 2 or 3 (for private licenes only).
MAXDISKSPACE="102400"					#Set the maximum allowed amount of disk space to use in megabyte for music+radio folder. (102400 mb is 100 GB(1024 mb = 1 GB))
BOOTTIME="30"							#This Time the Script is waiting at a Server Boot to starting the Bot
# --- edit variables END ---

#Do you will have a auto restart? Then you add the follow lines in the crontab without the (#)
#1 5 * * * /home/philippe/ts3bot/musikbot_1.sh restart
#Then the Bot restart Every Day at 5:01 Uhr
#
#whith this line the bot starting when the server starting:
#@reboot /home/philippe/ts3bot/musikbot_1.sh boot


#CHANGE IF YOU WANT BUT DON'T ASK WHY YOUR CONFIGURATION DOESN'T WORK
case "$1" in
	start)
		if whoami | grep -q -w $USER; then
			if screen -list | grep -q -w $SCREENNAME; then
				echo "TS3MusikBot is already running. try stop or restart"
				exit 1
			else
				screen -AmdS $SCREENNAME ./TS3MusicBot_runscript.sh -account $EMAIL -port $PORT -webif-pw $MASTERPW -webif-pw-user $USERPW -$QUERY -number $NUMBER -max-disk-space $MAXDISKSPACE
				sleep 10
				if screen -list | grep -q -w $SCREENNAME; then
					echo "TS3MusikBot is running"
				else echo "TS3MusikBot is not running!"
				fi
			fi
		else
			if su $USER -c "screen -list" | grep -q -w $SCREENNAME; then
				echo "TS3MusikBot is already running"
				exit 1
			else
				su $USER -c "screen -AmdS $SCREENNAME ./TS3MusicBot_runscript.sh -account $EMAIL -port $PORT -webif-pw $MASTERPW -webif-pw-user $USERPW -$QUERY -number $NUMBER -max-disk-space $MAXDISKSPACE"
				sleep 10
				if su $USER -c "screen -list" | grep -q -w $SCREENNAME; then
					echo "TS3MusikBot is running"
				else echo "TS3MusikBot is not running!"
				fi
			fi
		fi
	;;
	stop)
		if whoami | grep -q -w $USER; then
			screen -S $SCREENNAME -X stuff 'stop\n'
			sleep 5
			if screen -list | grep -q -w $SCREENNAME; then
				screen -p 0 -S $SCREENNAME -X quit
			fi
		else su $USER -c "screen -S $SCREENNAME -X stuff $'quit\n'"
			sleep 5
			if su $USER -c "screen -list" | grep -q -w $SCREENNAME; then
				su $USER -c "screen -p 0 -S $SCREENNAME -X quit"
			fi
		fi
		echo "TS3MusikBot is stopping"
	;;
	restart)
		echo "TS3MusikBot is restarting ..."
		$0 stop
		sleep 25
		$0 start
	;;
	status)
		if whoami | grep -q -w $USER; then
			if screen -list | grep -q -w $SCREENNAME; then
				echo "TS3MusikBot is running"
			else echo "TS3MusikBot is not running"
			fi
		elif su $USER -c "screen -list" | grep -q -w $SCREENNAME; then
			echo "TS3MusikBot is running"
		else echo "TS3MusikBot is not running"
		fi
	;;
	boot)
		# This is for Crontab @reboot
		sleep $BOOTTIME
		$0 start
	;;
	*)
		echo "Benutze: ${0} {start|stop|restart|status} made by philw95"
		exit 1
esac
exit 0
#THAT'S THE END OF THE CONFIG
#Made by philw95
